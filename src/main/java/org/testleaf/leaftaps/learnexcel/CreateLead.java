package org.testleaf.leaftaps.learnexcel;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateLead extends PreAndPost{

	
	@BeforeClass
	public void name() {
		
		excelFileName="CreateLead";
	}
	
	@Test(dataProvider="datasupplier")
	
public void setData(String companyName,String firstName,String lastName)
	
	{
		
		driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
	    driver.findElementById("createLeadForm_companyName").sendKeys(companyName);
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
		driver.findElementByName("submitButton").click();

			}
}