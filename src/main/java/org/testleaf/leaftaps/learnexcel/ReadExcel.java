package org.testleaf.leaftaps.learnexcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	
	
	public static String[][] getExcelData(String excelFileName) throws IOException {
		
		//open work book
		XSSFWorkbook wbook = new XSSFWorkbook("E:/practise/"+excelFileName+".xlsx");
		//get to sheet
		XSSFSheet sheet= wbook.getSheetAt(0);
		//get row count
		int rowcount = sheet.getLastRowNum();
		//get column count
		int columncount = sheet.getRow(0).getLastCellNum();
		
		String[][] data  = new String [rowcount][columncount];
		
		System.out.println(rowcount+" "+columncount);
		for (int i = 1; i <=rowcount; i++) {
			
			XSSFRow row = sheet.getRow(i);
			//each row
			
			for (int j = 0; j <columncount; j++) {
				
				//each column
				
				XSSFCell cell = row.getCell(j);
				
				//get string value
				
				String value = cell.getStringCellValue();
				
				data[i-1][j] = value;
				
				return data;
				
			}
			
		}
		return data;
		
	}

}
