package org.testleaf.leaftaps.learnexcel;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import org.testng.annotations.AfterMethod;




public class PreAndPost {
	
	public ChromeDriver driver;
	
	public String excelFileName;
	

	
	
	@BeforeMethod
	@Parameters({"username","password"})
	
	public void login(String username,String password) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys(username);
		driver.findElementById("password").sendKeys(password);
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@AfterMethod
	public void closeBrowser() {
		driver.close();
	}
	
	@DataProvider(name="datasupplier")
	
	public String[][] getData()throws IOException{
		
		String[][] data = ReadExcel.getExcelData(excelFileName);
		
		return data;
	}
	
	
	
	
	
}