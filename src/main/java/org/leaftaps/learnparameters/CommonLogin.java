package org.leaftaps.learnparameters;


	
	import java.util.concurrent.TimeUnit;


	import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
	
	public class CommonLogin {
		
		public ChromeDriver driver;
	
		

    @Parameters({"username","password"})
    @BeforeMethod
      public void login(String username,String password) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		    driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://leaftaps.com/opentaps/");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementById("username").sendKeys(username);
			driver.findElementById("password").sendKeys(password);
			driver.findElementByClassName("decorativeSubmit").click();
		}
      
	
      
	@AfterMethod
		public void closeBrowser() {
			
			driver.close();
		}
	}
	
	
		
		
		
		
	

