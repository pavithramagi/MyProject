package org.leaftaps.testcases;
//week1 day4 assignment

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class FindAndEditLeads {

	//@Test (dependsOnMethods="org.leaftaps.leads.CreateLeads.runCreateLeads", invocationCount=2)
	public void runEditLeads() throws InterruptedException {
	//public static void main(String[] args) throws InterruptedException {
		
		// TODO Auto-generated method stub
		
		//Establish connection between Selenium code and driver
		 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		 //Create object for driver class
		 ChromeDriver cdriver = new ChromeDriver();
		 //load the leaftaps url
		 cdriver.get("http://leaftaps.com/opentaps/");
//maximize the browser
		 cdriver.manage().window().maximize();
		//use implicit wait
			cdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 // Login
		 cdriver.findElementById("username").sendKeys("DemoSalesManager");
		 cdriver.findElementById("password").sendKeys("crmsfa");
		 cdriver.findElementByClassName("decorativeSubmit").click();
		 // Go to CRM/SFA page
		 cdriver.findElementByLinkText("CRM/SFA").click();
		 
		 cdriver.findElementByLinkText("Leads").click();

//Find Leads link
		 cdriver.findElementByLinkText("Find Leads").click();
		// WebElement xpathelement = cdriver.findElementByXPath("//label[text()='Lead ID:']");
		 
		  cdriver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Koushik");;
		//cdriver.findElementByLinkText("Find Leads").click();
		//button[@type='button']
		  cdriver.findElementByXPath("(//button[@type='button'])[7]").click();
		 // Thread.sleep(3000);
		  WebDriverWait wait  = new WebDriverWait(cdriver, 30);
			 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@type='button'])[7]")));
		cdriver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		String title= cdriver.getTitle();
		
		 System.out.println( title);
		 cdriver.findElementByLinkText("Edit").click();
		 WebElement ele1= cdriver.findElementByXPath("(//input[@name='companyName'])[2]");
		 String compname= ele1.getAttribute("value");
		 System.out.println(compname);
		 cdriver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
		 cdriver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("TestLeaf Solutions Ltd");
		 cdriver.findElementByXPath("//input[@type='submit']").click();;
		
		 WebElement ele2= cdriver.findElementByXPath("(//input[@name='companyName'])[2]");
		 String compname1= ele2.getAttribute("value");
		 
		 System.out.println(compname);
		 
		 
		if (compname.equals(compname1)){
			
			System.out.println("company name not changed");
		}else {
			System.out.println("company name has changed");
			cdriver.close();
}
	}
	}
