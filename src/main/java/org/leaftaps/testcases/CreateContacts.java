// Week1 day3 assignment - create a contact in leaftaps application and print the title of resultant page

package org.leaftaps.testcases;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class CreateContacts {
	@Test
	public void runCreatecontact() throws InterruptedException {
	//public static void main(String[] args) throws InterruptedException 
		//Establish connection between Selenium code and driver
		 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		 //Create object for driver class
		 ChromeDriver cdriver = new ChromeDriver();
		 //load the leaftaps url
		 cdriver.get("http://leaftaps.com/opentaps/");
//maximise the browser
		 cdriver.manage().window().maximize();
		 // Login
		 cdriver.findElementById("username").sendKeys("DemoSalesManager");
		 cdriver.findElementById("password").sendKeys("crmsfa");
		 cdriver.findElementByClassName("decorativeSubmit").click();
		
		 
		 // Go to CRM/SFA page
		 cdriver.findElementByLinkText("CRM/SFA").click();
		 cdriver.findElementByLinkText("Contacts").click();
		 cdriver.findElementByLinkText("Create Contact").click();
		 
		 // fill details in Create Contact page
		 cdriver.findElementById("firstNameField").sendKeys("Sampanna");
		 cdriver.findElementById("lastNameField").sendKeys("T");
		 cdriver.findElementById("createContactForm_preferredCurrencyUomId").sendKeys("DZD - Algerian Dinar");
		 cdriver.findElementById("createContactForm_description").sendKeys("Creating user contact");
		 cdriver.findElementById("createContactForm_primaryPhoneAreaCode").sendKeys("001");
		 cdriver.findElementById("createContactForm_primaryPhoneNumber").sendKeys("1234567891");
		 cdriver.findElementById("createContactForm_primaryEmail").sendKeys("sampanna@gmail.com");
		 cdriver.findElementById("createContactForm_generalAddress1").sendKeys("Murugu Nagar");
		 cdriver.findElementById("createContactForm_generalAddress2").sendKeys("Velachery");
		 cdriver.findElementById("createContactForm_generalCity").sendKeys("Chennai");
		 cdriver.findElementById("createContactForm_generalPostalCode").sendKeys("600042");
		 cdriver.findElementById("createContactForm_generalCountryGeoId").sendKeys("India");
		 //Thread.sleep(6000);
		 WebDriverWait wait  = new WebDriverWait(cdriver, 30);
		 wait.until(ExpectedConditions.elementToBeClickable(locator))
		 cdriver.findElementById("createContactForm_generalStateProvinceGeoId").sendKeys("TAMILNADU");
		 cdriver.findElementByClassName("smallSubmit").click();
		 System.out.println(cdriver.getTitle());
		 //
		 //
		 //
		 
		 }

}



