package org.leaftaps.testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class CreateLeadRef {
@Test
public void runCreateLeads() throws InterruptedException{
	//public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		//Establish connection between Selenium code and driver
		//Establish connection between Selenium code and driver
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
 //Create object for driver class
 ChromeDriver cdriver = new ChromeDriver();
 //load the leaftaps url
 cdriver.get("http://leaftaps.com/opentaps/");
//maximise the browser
 cdriver.manage().window().maximize();
//use implict wait
	cdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		 // Login
		 cdriver.findElementById("username").sendKeys("DemoSalesManager");
		 cdriver.findElementById("password").sendKeys("crmsfa");
		 cdriver.findElementByClassName("decorativeSubmit").click();
		 Thread.sleep(2000);
		 
		 // Go to CRM/SFA page
		 cdriver.findElementByLinkText("CRM/SFA").click();
		 
		 
		 cdriver.findElementByLinkText("Leads").click();
		 cdriver.findElementByLinkText("Create Lead").click();;
		 //CreateLEad page
		 cdriver.findElementById("createLeadForm_companyName").sendKeys("TestLeafTraining");
		 cdriver.findElementById("createLeadForm_firstName").sendKeys("Sampanna");
		 cdriver.findElementById("createLeadForm_lastName").sendKeys("T");
		 
		 //to get dropdown object
		 WebElement source = cdriver.findElementById("createLeadForm_dataSourceId"); // ctrl 2 release L
		  //to assign Select object
		 Select dd = new Select(source);
		 //To get the dropdown Source value by visible Text
		 dd.selectByVisibleText("Direct Mail");
		 
		 
		 WebElement mc = cdriver.findElementById("createLeadForm_marketingCampaignId"); // ctrl 2 release L
		  //to assign Select object
		 Select mc1 = new Select(mc);
		 //To get the list of elements from dropdown
		 List<WebElement> allvalues = mc1.getOptions();
		 //To get the total count of the dropdown list and use to select a text
		 
		 mc1.selectByIndex(allvalues.size() -1); 
		 
		 
		 cdriver.findElementByClassName("smallSubmit").click();
		 
	}

}

	


