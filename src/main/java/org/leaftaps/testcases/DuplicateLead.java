package org.leaftaps.testcases;







import org.testng.annotations.Test;

import org.leaftaps.testcases.PreAndPost;

public class DuplicateLead extends PreAndPost{
	
	
	@Test
	public void runDuplicateLead() throws InterruptedException {
		
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//span[text()='Phone']").click();
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("99");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.findElementByLinkText("Duplicate Lead").click();
		driver.findElementByName("submitButton").click();
//		driver.close();
	}
}