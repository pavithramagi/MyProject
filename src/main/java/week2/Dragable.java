package week2;



	import java.util.List;

	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.interactions.Actions;
	import org.testng.annotations.Test;

	public class Dragable {
	@Test
		public  void leafground() throws InterruptedException {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverx.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			
			driver.get("http://leafground.com/");
			
//			driver.findElementByLinkText("Draggable").click();
//			Actions builder = new Actions(driver);
//			builder.dragAndDropBy(driver.findElementById("draggable"), 100, 100).perform();
//			
//			driver.navigate().back();
//			driver.findElementByLinkText("Droppable").click();
//			builder.dragAndDrop(driver.findElementById("draggable"), driver.findElementById("droppable")).perform();
//			
//			driver.navigate().back();
//			driver.findElementByLinkText("Selectable").click();
//			builder.clickAndHold(driver.findElementByLinkText("Item 1")).clickAndHold(driver.findElementByLinkText("Item 2")).clickAndHold(driver.findElementByLinkText("Item 3")).perform();
//			
//			driver.navigate().back();
			
			driver.findElementByLinkText("Table").click();
			Thread.sleep(2000);
			List<WebElement> tableC = driver.findElementsByXPath("//table[@cellspacing=\"0\"]//tr[2]//td");
			System.out.println(tableC.size());
			List<WebElement> tableR = driver.findElementsByXPath("//table[@cellspacing=\"0\"]//tr");
			System.out.println(tableR.size());
			
			List<WebElement> progress = driver.findElementsByXPath("//table[@cellspacing=\"0\"]//tr//td[2]");
			for (WebElement webElement : progress) {
				String prob = webElement.getText();
				System.out.println(prob);
			}
			String pp = driver.findElementByXPath("//font[contains(text(),'Learn to interact with ')]/following::td[1]").getText();
			System.out.println("Learn to interact with Elements	"+pp);
			driver.close();
		}

	}