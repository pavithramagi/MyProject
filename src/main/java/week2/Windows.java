package week2;



	import java.util.ArrayList;
	import java.util.List;
	import java.util.Set;

	import org.openqa.selenium.chrome.ChromeDriver;

	public class Windows {

		public static void main(String[] args) {

			
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverx.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			
			driver.get("http://leafground.com/pages/Window.html");
			driver.findElementByXPath("//button[text()=\"Open Multiple Windows\"]").click();
			
			Set<String> listWindows = driver.getWindowHandles();
//			List<String> wList = new ArrayList<>(listWindows);
			
			for (String string : listWindows) {
				
			
			driver.switchTo().window(string);
			String title = driver.getTitle();
			System.out.println(title);
			}
			
			driver.quit();
		}
		

	}