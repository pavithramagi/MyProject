package week2;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.Alert;

public class Frames {

	        public static void main(String[] args) throws InterruptedException {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverx.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			
			driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
			driver.switchTo().frame("iframeResult");
			
			driver.findElementByXPath("//button[text()='Try it']").click();

			driver.switchTo().alert().sendKeys("Rathna Prashanth");
			Thread.sleep(2000);
			driver.switchTo().alert().accept();
			
			String name = driver.findElementById("demo").getText();
			String[] sp = name.split("!");
			System.out.println(sp[1]);
			String[] sp1 = sp[0].split(" ");
			System.out.println(sp1[1]+" "+sp[0]);
			driver.close();
			
			
		}

	
	}


