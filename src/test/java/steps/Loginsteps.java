package steps;



import org.openqa.selenium.chrome.ChromeDriver;
//import org.testleaf.teaftaps.testcases.LoginLogin;
import org.testleaf.teaftaps.testcases.LoginLogin;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Loginsteps extends LoginLogin {
	
	
//	@Given("Start application") 
//	public void startApplication() {
//		
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//		driver= new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("http://leaftaps.com/opentaps/");
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//
//		}
	@Given("Enter the username as (.*)")
	public void enterTheUsernameAsDemosalesmanager(String uName) {
		
		driver.findElementById("username").sendKeys(uName);
		
	}
	
	@Given("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pass) {
		
		driver.findElementById("password").sendKeys(pass);
		
	}
	
	
	@When("I click on the login button")
	public void iClickOnTheLoginButton() {
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		
	}
	@Then("Verify login is success")
	
	public void verifyLoginIsSuccess() {
		
		System.out.println("Login Success");
		
	}

	
	}


	