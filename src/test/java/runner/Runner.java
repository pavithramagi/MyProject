package runner;



import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.SnippetType;

@CucumberOptions(
	
	features = "src\\test\\java\\login",
	glue= {"steps","hooks"},
	dryRun = false,
	snippets = SnippetType.CAMELCASE,
	monochrome = true,
	plugin= {"pretty"}
	

)



public class Runner extends AbstractTestNGCucumberTests {

}
